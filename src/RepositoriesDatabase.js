"use strict";
var ReverseIndex = require("./ReverseIndex.js");
var fs = require("fs");
var URL = require("url").URL;

/*
 * Helper functions
 */
function splitNameIntoTokens(str) {
    return str.split("/").concat([str]);
}

function splitDescriptionIntoTokens(str) {
    return str.split(/(\s|\/)+/);
}

function createSearchIndex(index) {
    var reverseIndex = new ReverseIndex();

    index.forEach(function(value, id) {
        if (value.verified) {
            var tokens = splitNameIntoTokens(value.name.toLowerCase()).concat(value.topics.map(function(s) {
                return s.toLowerCase();
            }), splitDescriptionIntoTokens(value.description.toLowerCase()));
            reverseIndex.add(id, tokens);
        }
    });

    return reverseIndex;
}

function validateAndCheckIfIsOnKnowGitHost(repositoryUrl) {
    try {
        var url = new URL(repositoryUrl);
        if ((url.host === "github.com" || url.host === "gitlab.com") && url.protocol === "https:" && url.pathname.length > 4 && url.pathname.substr(url.pathname.length - 4) === ".git") {
            return true;
        } else {
            return false;
        }
    } catch (e) {
        return false;
    }
}

/*
 * Constructor
 */
function RepositoriesDatabase() {
    this._index = [];
    this._knownRepositories = new Set();
    this._reverseIndex = createSearchIndex(this._index);
}

/*
 * Methods
 */
RepositoriesDatabase.prototype.load = function(filePath) {
    // load index from file
    var self = this;
    return new Promise(function(resolve, reject) {
        fs.readFile(filePath, {
            encoding: "utf8"
        }, function(err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    }).then(function(data) {
        return data;
    }, function(e) {
        return "[]";
    }).then(function(data) {
        self._index = JSON.parse(data);
        self._index.forEach(function(value) {
            self._knownRepositories.add(value.url);
        });
    });
};

RepositoriesDatabase.prototype.store = function(filePath) {
    // store index to file
    var self = this;
    return new Promise(function(resolve, reject) {
        fs.writeFile(filePath, JSON.stringify(self._index), {
            encoding: "utf8"
        }, function(err) {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

RepositoriesDatabase.prototype.addToIndex = function(repositoryUrl) {
    // add repo to index if possible
    var self = this;
    return new Promise(function(resolve) {
        if (self._knownRepositories.has(repositoryUrl)) {
            for (var i = 0; i < self._index.length; ++i) {
                if (self._index[i].url === repositoryUrl) {
                    self._index[i].popularity++;
                    break;
                }
            }
            resolve("indexed");
        } else if (validateAndCheckIfIsOnKnowGitHost(repositoryUrl)) {
            self._knownRepositories.add(repositoryUrl);
            self._index.push({
                url: repositoryUrl,
                name: "",
                topics: [],
                description: "",
                popularity: 1,
                verified: false,
                invalid: false
            });
            resolve("scheduled");
        } else {
            resolve("not accepted");
        }
    });
};

RepositoriesDatabase.prototype.updateReverseIndex = function() {
    // update reverse index based on index
    var self = this;
    return new Promise(function(resolve) {
        self._reverseIndex = createSearchIndex(self._index);
        resolve();
    });
};

RepositoriesDatabase.prototype.updateContents = function(updater) {
    // update index contents
    // helpers:
    function updateSingleRepo(updater, index, currentPosition, stats) {
        return updater(index[currentPosition].url).then(function(data) {
            if (data) {
                var entry = index[currentPosition];

                if (entry.verified === false) {
                    stats.verified++;
                }
                entry.name = data.name;
                entry.topics = data.topics;
                entry.description = data.description;
                entry.verified = true;
                stats.updated++;
            } else {
                stats.connectionFailures++;
            }
            // data === undefined means that hosting server not respond
        }, function(error) {
            stats.invalidated++;
            index[currentPosition].invalid = true;
        });
    }

    function updateRepos(updater, index, currentPosition, maxPosition, stats) {
        if (currentPosition < maxPosition) {
            return updateSingleRepo(updater, index, currentPosition, stats).then(function() {
                return updateRepos(updater, index, currentPosition + 1, maxPosition, stats);
            });
        } else {
            return Promise.resolve();
        }
    }

    // finally update:
    var self = this;
    var stats = {
        verified: 0,
        updated: 0,
        connectionFailures: 0,
        invalidated: 0,
        oldIndexSize: self._index.length,
        newIndexSize: 0
    };
    return updateRepos(updater, self._index, 0, self._index.length, stats).then(function() {
        self._index.forEach(function(entry) {
            if (entry.invalid) {
                self._knownRepositories.delete(entry.url);
            }
        });

        self._index = self._index.filter(function(entry) {
            return !entry.invalid;
        });
        stats.newIndexSize = self._index.length;
        return stats;
    });
};

function searchResultSort(a, b) {
    if (a.popularity > b.popularity) {
        return -1;
    } else if (a.popularity < b.popularity) {
        return 1;
    } else {
        var anl = a.name.toLowerCase();
        var bnl = b.name.toLowerCase();
        if (anl < bnl) {
            return -1;
        } else if (anl > bnl) {
            return 1;
        } else {
            return 0;
        }
    }
}

RepositoriesDatabase.prototype.search = function(phrase) {
    var self = this;
    return new Promise(function(resolve) {
        resolve(self._reverseIndex.search(phrase.toLowerCase().split(/\s+/)));
    }).then(function(results) {
        return results.slice(0, 50).map(function(result) {
            var entry = self._index[result.ref];
            return {
                url: entry.url,
                name: entry.name,
                topics: entry.topics,
                description: entry.description,
                popularity: entry.popularity
            };
        }).sort(searchResultSort);
    });
};

module.exports = RepositoriesDatabase;