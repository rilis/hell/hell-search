"use strict";

function calculateTokenSubtokensWithValue(token) {
    var subtokens = {};
    subtokens[token] = 100;
    for (var i = 4; i < token.length; i += 2) {
        subtokens[token.substr(0, i)] = (100 * i) / token.length;
    }
    return subtokens;
}

function calculateTokensSubtokensWithValue(tokens) {
    var subtokens = tokens.map(calculateTokenSubtokensWithValue).reduce(function(acc, subtokens) {
        Object.keys(subtokens).forEach(function(subtoken) {
            if (acc.hasOwnProperty(subtoken)) {
                acc[subtoken].value += subtokens[subtoken];
                acc[subtoken].count++;
            } else {
                acc[subtoken] = {
                    value: subtokens[subtoken],
                    count: 1
                };
            }
        });
        return acc;
    }, {});

    Object.keys(subtokens).forEach(function(subtoken) {
        subtokens[subtoken] = subtokens[subtoken].value / subtokens[subtoken].count;
    });
    return subtokens;
}

function getSubtokens(tokens) {
    var subtokens = {};
    tokens.forEach(function(token) {
        subtokens[token] = 0;
        for (var i = 4; i < token.length; i += 2) {
            subtokens[token.substr(0, i)] = 0;
        }
    });

    return Object.keys(subtokens);
}

function ReverseIndex(config) {
    this._index = {};
}

ReverseIndex.prototype.add = function(id, documentTokens) {
    var idx = this._index;
    getSubtokens(documentTokens).forEach(function(subtoken) {
        if (idx.hasOwnProperty(subtoken)) {
            idx[subtoken].push(id);
        } else {
            idx[subtoken] = [id];
        }
    });
};

ReverseIndex.prototype.search = function(tokens) {
    var searchHints = calculateTokensSubtokensWithValue(tokens);

    var foundDocuments = {};
    var idx = this._index;
    Object.keys(searchHints).forEach(function(searchHint) {
        if (idx.hasOwnProperty(searchHint)) {
            var founds = idx[searchHint];
            founds.forEach(function(documentId) {
                if (foundDocuments.hasOwnProperty(documentId)) {
                    foundDocuments[documentId] += searchHints[searchHint];
                } else {
                    foundDocuments[documentId] = searchHints[searchHint];
                }
            });
        }
    });
    var results = [];
    Object.keys(foundDocuments).forEach(function(result) {
        results.push({
            ref: result,
            score: foundDocuments[result]
        });
    });
    results.sort(function(result1, result2) {
        return result2.score - result1.score;
    });
    return results;
};

module.exports = ReverseIndex;