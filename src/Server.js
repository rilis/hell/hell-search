"use strict";
var http = require("http");

function Server(handler) {
    this._impl = null;
    this._handler = handler;
}

function fetachRequest(req) {
    return new Promise(function(resolve, reject) {
        if (req.method === "POST") {
            var body = "";
            req.on("data", function(data) {
                body += data;
            });
            req.on("end", function() {
                resolve(body);
            });
            req.on("error", function() {
                reject();
            });
        } else {
            reject();
        }
    }).then(function(data) {
        return {
            data: JSON.parse(data),
            url: req.url
        };
    });
}

Server.prototype.run = function(port) {
    var self = this;
    return new Promise(function(resolve, reject) {
        self._impl = http.createServer(function(request, response) {
            fetachRequest(request)
                .then(function(requestData) {
                    return self._handler(requestData);
                })
                .then(function(data) {
                    return JSON.stringify(data);
                })
                .then(function(data) {
                    response.writeHead(200, {
                        'Content-type': 'application/json'
                    });
                    response.end(data, "utf8");
                }).catch(function(e) {
                    response.writeHead(418, {
                        'Content-type': 'application/json'
                    });
                    response.end("[{\"o\":\"O\"}]", "utf8");
                });
        });
        self._impl.listen(port, "127.0.0.1", function(err) {
            if (err) {
                reject();
            } else {
                resolve();
            }
        });
    }).then(function() {
        console.log("[%s] server started.", new Date().toUTCString());
        return new Promise(function(reject) {
            self._impl.on("close", reject);
        });
    });
};

module.exports = Server;