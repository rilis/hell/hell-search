"use strict";
var RepositoriesDatabase = require("./RepositoriesDatabase.js");
var updater = require("./updater.js");
var Server = require("./Server.js");
var path = require("path");

var handledRequests = 0;

function promiseTimeout(duration) {
    return new Promise(function(resolve) {
        setTimeout(resolve, duration);
    });
}

function exitWithErrorIfComplete(toWatch, taskName, exitcode) {
    toWatch.then(function() {
        console.log("hell-search(%s) should never stop!", taskName);
        process.exit(exitcode);
    }, function(err) {
        console.log("hell-search(%s) should never stop!", taskName);
        console.log(err);
        process.exit(exitcode);
    });
}

function requestHandler(rdb, request) {
    return new Promise(function(resolve, reject) {
        handledRequests++;
        if (request.url === "/search") {
            rdb.search(request.data.request).then(resolve, reject);
        } else if (request.url === "/publish") {
            Promise.all(request.data.map(function(repoUrl) {
                return rdb.addToIndex(repoUrl).then(function(status) {
                    return {
                        url: repoUrl,
                        status: status
                    };
                });
            })).then(resolve, reject);
        } else {
            reject();
        }
    });
}

function scheduleServer(rdb) {
    return new Promise(function(resolve) {
        resolve(new Server(function(requestData) {
            return requestHandler(rdb, requestData);
        }));
    }).then(function(server) {
        return server.run(1234);
    });
}

function scheduleDatabaseUpdate(rdb, updater, indexDbFilePath) {
    console.log("[%s] handled requests since last update: %d", new Date().toUTCString(), handledRequests);
    handledRequests = 0;
    return Promise.all([rdb.updateContents(updater).then(function(stats) {
        console.log("[%s] updateContents completed: %j.", new Date().toUTCString(), stats);
        return rdb.updateReverseIndex();
    }).then(function() {
        console.log("[%s] updateReverseIndex completed.", new Date().toUTCString());
        return rdb.store(indexDbFilePath);
    }).then(function() {
        console.log("[%s] store completed.", new Date().toUTCString());
    }), promiseTimeout(1 * 60 * 60 * 1000 /*1h*/ )]).then(function() {
        console.log("[%s] starting update...", new Date().toUTCString());
        return scheduleDatabaseUpdate(rdb, updater, indexDbFilePath);
    });
}

function hellSearch(args) {
    var indexDbFilePath = path.join(process.cwd(), "hs.json");
    console.log("[%s] starting(%s)...", new Date().toUTCString(), indexDbFilePath);


    var rdb = new RepositoriesDatabase();
    rdb.load(indexDbFilePath).then(function() {
        console.log("[%s] load completed.", new Date().toUTCString());
        return rdb.updateReverseIndex();
    }).then(function() {
        exitWithErrorIfComplete(scheduleDatabaseUpdate(rdb, updater, indexDbFilePath), "DatabaseUpdate", -2);
        exitWithErrorIfComplete(scheduleServer(rdb), "server", -3);
        console.log("[%s] ready and running...", new Date().toUTCString());
    }).catch(function(e) {
        console.error(e);
        process.exit(-4);
    });
}

module.exports = hellSearch;