"use strict";
var URL = require("url").URL;
var https = require("https");

function promiseGetRequest(requestConfig) {
    return new Promise(function(resolve, reject) {
        var url = new URL(requestConfig.url);
        var options = {
            host: url.host,
            path: url.pathname + url.search + url.hash,
            method: 'GET',
            headers: requestConfig.headers
        };

        var req = https.request(options, function(res) {
            res.on('error', function(err) {
                reject();
            });
            var data = "";
            res.setEncoding('utf8');
            res.on('data', function(chunk) {
                data += chunk;
            });
            res.on('end', function() {
                resolve(data);
            });
        });

        req.on('error', function(err) {
            reject();
        });
        req.end();
    });
}

function getRepoInfoFromGithub(repoUrl) {
    return new Promise(function(resolve, reject) {
        var repoName = repoUrl.pathname.substr(1, repoUrl.pathname.length - 5);
        return promiseGetRequest({
            url: "https://api.github.com/repos/" + repoName,
            headers: {
                "Accept": "application/vnd.github.mercy-preview+json",
                "User-Agent": "hell-search"
            }
        }).then(function(body) {
            return JSON.parse(body);
        }).then(function(parsed) {
            if (parsed.full_name === repoName) {
                resolve({
                    name: parsed.full_name ? parsed.full_name : "",
                    topics: parsed.topics && Array.isArray(parsed.topics) ? parsed.topics : [],
                    description: parsed.description ? parsed.description : ""
                });
            } else {
                reject();
            }
        }, function() {
            resolve();
        });
    });
}

function getRepoInfoFromGitlab(repoUrl) {
    return new Promise(function(resolve, reject) {
        var repoName = repoUrl.pathname.substr(1, repoUrl.pathname.length - 5);
        return promiseGetRequest({
            url: "https://gitlab.com/api/v4/projects/" + encodeURIComponent(repoName),
            headers: {
                "User-Agent": "hell-search"
            }
        }).then(function(body) {
            return JSON.parse(body);
        }).then(function(parsed) {
            if (parsed.path_with_namespace === repoName) {
                resolve({
                    name: parsed.path_with_namespace ? parsed.path_with_namespace : "",
                    topics: parsed.tag_list && Array.isArray(parsed.tag_list) ? parsed.tag_list : [],
                    description: parsed.description ? parsed.description : ""
                });
            } else {
                reject();
            }
        }, function(err) {
            resolve();
        });
    });
}

function updater(repoUrl) {
    return new Promise(function(resolve) {
        var url = new URL(repoUrl);
        resolve(url);
    }).then(function(url) {
        if (url.host === "github.com") {
            return getRepoInfoFromGithub(url);
        } else {
            return getRepoInfoFromGitlab(url);
        }
    });
}

module.exports = updater;